# Création de vidéos de projets

Une vidéo est sans doute la seule chose qui restera sur le long terme de votre projet. C'est aussi une des meilleures façons de valoriser votre travail (dans un CV par exemple). Ne négligez donc pas son importance.

## Enregistrement de vidéos d'écran
 * Linux: Utilisation de [Kazam Screencaster](https://launchpad.net/kazam), installé sur les machines des salles de TP
 * OS X: Utilisation de QuickTime (pas besoin de la version pro). `File > New screen recording`
 * Windows: Si vous possédez une carte NVidia, vous pouvez utiliser [NVIDIA GeForce Experience](http://www.geforce.com/geforce-experience/share)
 * Multi-plateformes: [OBS Studio](https://obsproject.com/)
  
 Si vous connaissez d'autres logicels performants et gratuits, [n'hésitez pas à me contacter](mailto:gery.casiez@univ-lille.fr).

## Création du début et de la fin de la vidéos

Ouvrez un des modèles de [diaporamas disponibles](https://gitlab.univ-lille.fr/gery.casiez/videomaker) et adaptez le à votre projet, en modifiant:
 
 * Le titre
 * Les auteurs
 * Le titre de la matière et le numéro du semestre
 * Votre promotion

 La première chose à faire est d'ajuster la taille des diapos à celle de votre vidéo. Avec libreOffice, allez dans Format > Page et indiquez la largeur et hauteur en pixels (par exemple `1200px`)

 Exportez les diapos de votre présentation au format png ou jpeg (Avec LibreOffice, `Fichier > Export`)

## Installer le script
Téléchargez le script [videomaker](https://gitlab.univ-lille.fr/gery.casiez/videomaker/-/blob/master/videomaker) ou utilisez directement les commandes suivantes:

```
wget https://gitlab.univ-lille.fr/gery.casiez/videomaker/-/raw/master/videomaker
chmod +x videomaker
```

Le script fonctionne sous Linux et MacOS. Le script utilise les commandes `ffmpeg` et `imagemagick` que vous devez éventuellement installer au préalable avec la commande `sudo apt-get install ffmpeg imagemagick` sous Linux.
Si la commande retourne une erreur, commencez par ajouter le repository qui contient ffmpeg avec les commandes suivantes:

```
sudo add-apt-repository ppa:mc3man/trusty-media
sudo apt-get update
sudo apt-get dist-upgrade
```

Sous macOS, vous pouvez installer ces commandes avec Homebrew ou Macport. Installez [HomeBrew](http://brew.sh/) puis lancez la commande `brew install ffmpeg imagemagick`. Pour [MacPort](https://www.macports.org/), commencez par l'installer puis en utilisez la commande `sudo port install ffmpeg imagemagick`.


## Compilez votre vidéo

Le script prend trois arguments: votre vidéo, l'image de début et celle de fin. Il produira en retour une vidéo au format H.264 avec le tout. Vous devez obtenir un fichier qui contient dans le nom PROCESSED.mp4

Exemple d'utilisation du script :
```
./videomaker video.mov ImageDebut.jpeg ImageFin.jpeg
```

## Ajout de sous-titres

 Pour ajouter des commentaires dans votre vidéo, vous pouvez ajouter un fichier de sous-titres au format [SRT](https://matroska.org/technical/specs/subtitles/srt.html). Un fichier srt est un simple fichier texte qui comprend les sous-titres ainsi que les instants où ils apparaissent et disparaissent. Si vous donnez exactement le même nom de fichier au fichier srt que votre vidéo (sans l'extension bien sûr), la vidéo et les sous-titres peuvent être lus directement par des logiciels comme [VLC](http://www.videolan.org).


